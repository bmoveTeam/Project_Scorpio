// Cuando se baja de 20px del top de la web y la pantalla sea mas ancha de 600px el boton sale
window.onscroll = function() {scrollFunction()}; //Funcion de scroll
function scrollFunction() {
  if (document.documentElement.scrollTop > 20 && window.screen.width > 600) { //El boton sale en web, en movil no
    document.getElementById("btnup").style.display = "block";
} else {
    document.getElementById("btnup").style.display = "none";
} 
}
function detectmob() {
   if(window.innerHeight <= 600) {
    document.getElementById("btnup").style.display = "none";
   }
}

// Cuando se pulsa el boton ir arriba
function topFunction() {
    document.documentElement.scrollTop = 0; // Para Chrome, Firefox, IE y Opera
}
